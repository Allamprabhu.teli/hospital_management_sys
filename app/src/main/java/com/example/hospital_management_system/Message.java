package com.example.hospital_management_system;
import android.content.Context;
import android.widget.Toast;
public class Message {
    public static void message(Context context,String message)
    {
        Toast.makeText(context,message,Toast.LENGTH_LONG).show();
    }
}
